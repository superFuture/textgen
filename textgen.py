#!/usr/bin/env python

import os, sys
from PIL import Image
import pytesseract

text_extension = ".txt"
lang = 'fra'
delimeter = "-----------------------"

def get_text(image_path):
    print("start ocr in",lang+":",image_path)
    text = pytesseract.image_to_string(image_path, lang=lang)
    text_file = image_path.split(".")[0]+text_extension
    file = open(text_file, "w")
    file.write(text)
    print("saved text: ",text_file)

def rotate_image(image_path):
    with Image.open(image_path) as img:
        img = img.rotate(-90, expand=True)
        img.save(image_path)
        print("rotated image: ",image_path)

def rotate_images_in_folder(folder_path):
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            if file.endswith(('.jpg', '.jpeg', '.png', '.gif')):
                image_path = os.path.join(root, file)
                rotate_image(image_path)
                get_text(image_path)
                print(delimeter)

if __name__ == "__main__":

    folder_path = ""  # Replace with the path to your folder containing images

    if len(sys.argv) <= 1:
        print ("please add a folder ")
        sys.exit(1)
    else:
        folder_path = sys.argv[1]
    print() 
    print() 
    print("░░░  ░░░░  ░░        ░░  ░░░░  ░░░░░░░░░      ░░░   ░░░  ░░       ░░░  ░░░░  ░░░")
    print("▒▒▒  ▒▒▒▒  ▒▒  ▒▒▒▒▒▒▒▒▒  ▒▒  ▒▒▒▒▒▒▒▒▒  ▒▒▒▒  ▒▒    ▒▒  ▒▒  ▒▒▒▒  ▒▒▒  ▒▒  ▒▒▒▒")
    print("▓▓▓        ▓▓      ▓▓▓▓▓▓    ▓▓▓▓▓▓▓▓▓▓  ▓▓▓▓  ▓▓  ▓  ▓  ▓▓  ▓▓▓▓  ▓▓▓▓    ▓▓▓▓▓")
    print("███  ████  ██  ███████████  ███████████        ██  ██    ██  ████  █████  ██████")
    print("███  ████  ██        █████  ███████████  ████  ██  ███   ██       ██████  ██████")
    print()                                                                        
    print()                                                                        

    rotate_images_in_folder(folder_path)

